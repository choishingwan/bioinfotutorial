# Introduction

Writing and organizing analytical scripts can be challenging. 
Implementing and writing reusable and scalable pipeline are even more difficult. 

Here, I will try to go through an evolution of a polygenic risk score pipeline.
We will not explain the details of analytic steps, but rather, would focus on the actual scripting. 
Hopefully, in this process, I can guide you through the developmental process such that you will also be able to write a reusable and scalable pipeline at ease.


!!! Note "Prerequisites"
    You need to know how to use the terminal

## Table of Content

1. [Background](getting_started.md)
2. [Perform basic polygenic risk score analysis with `PRSice`](running_prsice.md)
3. [Using different quality control thresholds](different_qc.md)
4. [Introducing Nextflow](nextflow_intro.md)



