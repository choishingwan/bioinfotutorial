# Introduction
It is not uncommon that different quanlity control thresholds are explored in an analysis. 

In this section, we will take script developed in the [previous section](running_prsice.md) and try and repeat the analysis using different quality control thresholds.

## Learning Objectives
1. Understand importance of workspace hygiene
2. Submitting jobs to HPC servers
3. Array jobs and variance

## Required data and software
Same as [previous section](running_prsice.md#required-data).

## Using different minor allele frequency
Using our previous script, we can modify the script so that we can accept different minor allele frequency as a QC parameter:

```bash
#!/usr/bin/env bash
usage(){
    cat << EOF
Usage: ${0}
Run a simple polygenic risk score pipeline.
It will first filter out SNPs with MAF < 0.05, hwe < 1e-6 and genotype missingness > 0.01, then run PRSice using the provided summary statistic, phenotype and all covariates in the covariate file

    -p    PLINK executable
    -b    Genotype file prefix
    -r    PRSice executable
    -s    Summary statistic file with the following columns: SNP, P, OR, A1 and A2
    -f    Phenotype file. Assume third column is the phenotype
    -c    Covariate file. All covariates in this file will be used
    -m    Minor allele frequency threshold
    -o    Output prefix
    
EOF
}
while getopts p:b:r:s:f:o:c:m:h flag
do
    case "${flag}" in
        p) plink=${OPTARG};;
        b) bfile=${OPTARG};;
        r) prsice=${OPTARG};;
        s) sumstat=${OPTARG};;
        f) pheno=${OPTARG};;
        c) cov=${OPTARG};;
        o) prefix=${OPTARG};;
        m) maf=${OPTARG};;
        h) usage
        exit 0
        ;;
    esac
done

${plink} \
    --bfile ${bfile} \
    --maf ${maf} \
    --hwe 1e-6 \
    --geno 0.01 \
    --make-just-fam \
    --make-snplist \
    --out ${prefix}.QC

${prsice} \
    --base ${sumstat} \
    --target ${bfile} \
    --binary-target F \
    --pheno ${pheno} \
    --cov ${cov} \
    --base-maf MAF:0.01 \
    --stat OR \
    --or \
    --out ${prefix} \
    --keep ${prefix}.QC.fam \
    --extract ${prefix}.QC.snplist

gzip  ${prefix}.best
```

By adding the new `-m` parameter, our script now accept different MAF threshold as a QC parameter. 

??? question "There is a bug in this script, can you find it?"
    If we run the same script in the same folder multiple times, different runs will interfere with each other as the intermediates are hard-coded to have the same name. 
    We therefore cannot conclude if our analysis is valid. 

    We can modify the script such that our output file clearly state the QC we've performed:


    ```bash
    #!/usr/bin/env bash
    usage(){
        cat << EOF
    Usage: ${0}
    Run a simple polygenic risk score pipeline.
    It will first filter out SNPs with MAF < 0.05, hwe < 1e-6 and genotype missingness > 0.01, then run PRSice using the provided summary statistic, phenotype and all covariates in the covariate file

        -p    PLINK executable
        -b    Genotype file prefix
        -r    PRSice executable
        -s    Summary statistic file with the following columns: SNP, P, OR, A1 and A2
        -f    Phenotype file. Assume third column is the phenotype
        -c    Covariate file. All covariates in this file will be used
        -m    Minor allele frequency threshold
        -o    Output prefix
        
    EOF
    }
    while getopts p:b:r:s:f:o:c:m:h flag
    do
        case "${flag}" in
            p) plink=${OPTARG};;
            b) bfile=${OPTARG};;
            r) prsice=${OPTARG};;
            s) sumstat=${OPTARG};;
            f) pheno=${OPTARG};;
            c) cov=${OPTARG};;
            o) prefix=${OPTARG};;
            m) maf=${OPTARG};;
            h) usage
            exit 0
            ;;
        esac
    done

    ${plink} \
        --bfile ${bfile} \
        --maf ${maf} \
        --hwe 1e-6 \
        --geno 0.01 \
        --make-just-fam \
        --make-snplist \
        --out ${prefix}-${maf}.QC

    ${prsice} \
        --base ${sumstat} \
        --target ${bfile} \
        --binary-target F \
        --pheno ${pheno} \
        --cov ${cov} \
        --base-maf MAF:0.01 \
        --stat OR \
        --or \
        --out ${prefix}-${maf} \
        --keep ${prefix}-${maf}.QC.fam \
        --extract ${prefix}-${maf}.QC.snplist

    gzip  ${prefix}-${maf}.best
    ```

<p></p>
!!! Tips
    As our analyses and scripts grow in complexity, it is more likely for us to encounter bugs. 
    One of the most common kind of bugs is having intermediate files over-writing each other across multiple analyses.
    Therefore it is generally a good idea to run multiple analyses in separate folders. 
    Alternatively, we can use more sophisticated pipelines for that, which we will go into more detail in later chapters. 


## Submitting to Server
Most of the time, we would like to run our analyses on a high performance computer cluster (HPC), which require specific submission scripts. 
I will not go into detail of how to write a submission script, as you should be able to get those information from your HPC service provider. 
Rather, I am going touch on a few useful pattern that makes it easier for us to test multiple parameters together. 

!!! Note
    I will mainly focus on the lsf system here, which was the system implemented in our HPC. Though in theory, most of these pattern should be usable on most popular job management systems used by HPCs as long as they have an array job option.

Let's assume our `qc_and_run_prsice.sh` scripts is now ready to use and we want to test how using different minor allele frequency (MAF) affect our results. 
We might want to test a range of MAF e.g `0.001, 0.005, 0.01, 0.05, 0.1`. 
We can use the following submission script:

!!! note
    We assume `qc_and_run_prsice.sh` is now stored in the `scripts` folder

```bash
#BSUB -L /bin/sh      # Required header
#BSUB -n 1            # Use one node
#BSUB -J PRS          # Job name
#BSUB -q express      # Query type
#BSUB -W 12:00        # Wall time 
#BSUB -P acc_psychgen # Account name
#BSUB -o prs.o        # log file for normal output
#BSUB -eo prs.e       # log file for error output
#BSUB -M 20G          # Require memory

maf=0.001
project=/Users/sam/polygenic_score
# Generate the require file
mkdir -p ${project}/analysis/prs/${maf}
# Run analysis in the specific file
cd ${project}/analysis/prs/${maf}
# Run the actual script
sh  ${project}/scripts/qc_and_run_prsice.sh \
    -p ${project}/software/plink \
    -b ${project}/data/EUR  \
    -c ${project}/data/EUR.cov \
    -r ${project}/software/PRSice \
    -s ${project}/data/Height.QC.gz \
    -f ${project}/data/Eur.height \
    -m ${maf} \
    -o PRS-${maf}
```

We can run this script 5 times with different MAF value. 
Problem is, we often forgot which parameter we've already run, 
and this allow for human error. 
A better way might be to let the submission script to automatically try all 
required parameter.
This is where the array job comes into play.

### Array job submission
With most submission system, they allow for something called array job. 
Essentially, given the correct input, the job submission system will automatically generate a range of submission scripts. 

Take `lsf` as an example, if we replace `#BSUB -J PRS` with `#BSUB -J  PRS[1-10]`, we will run the same job script 10 times where a variable named `LSB_JOBINDEX` is set to equal the current array index. 

Back to our example, we can put the MAF parameters in a bash array, and ask our script to iterate each of the MAF thresholds as follow:
```bash
#BSUB -L /bin/sh      # Required header
#BSUB -n 1            # Use one node
#BSUB -J PRS[1-5]     # Job name
#BSUB -q express      # Query type
#BSUB -W 12:00        # Wall time 
#BSUB -P acc_psychgen # Account name
#BSUB -o prs.o%J.%I   # log file for normal output. 
# %J represents the job ID and 
# %I represents the array index
#BSUB -eo prs.e%J.%I  # log file for error output
#BSUB -M 20G          # Require memory

param=( dummy 0.001 0.005 0.01 0.05 0.1 )
maf=${param[${LSB_JOBINDEX}]}
project=/Users/sam/polygenic_score
# Generate the require file
mkdir -p ${project}/analysis/prs/${maf}
# Run analysis in the specific file
cd ${project}/analysis/prs/${maf}
# Run the actual script
sh  ${project}/scripts/qc_and_run_prsice.sh \
    -p ${project}/software/plink \
    -b ${project}/data/EUR  \
    -c ${project}/data/EUR.cov \
    -r ${project}/software/PRSice \
    -s ${project}/data/Height.QC.gz \
    -f ${project}/data/Eur.height \
    -m ${maf} \
    -o PRS-${maf}
```
This script will automatically submit 5 jobs, each with a different MAF threshold to the cluster. 

!!! Tips
    You might see that I have added a dummy variable in the job array.
    This is because the array jobs has index starting from 1, whereas the 
    bash array has index starting from 0. 
    If we don't add dummy at the front of the array, the first item in the 
    array will never be run, as the first every `${LSB_JOBINDEX}` will always be
    1.

!!! Important
    Spacing are surprisingly important for bash scripts.
    Always include the appropriate number of space in your array definition (right after `(` and before `)`), and don't put any spaces around `=` for variable definition. 

!!! Tips
    You can limit the number of concurrent jobs with the following syntax `#BSUB -J  PRS[1-10]%2`. 
    This syntax means that you only want two of the array job to run at the same time. 
    Always be mindful of how many resources you are using.
    If you are running large amount of jobs, maybe try leaving some resources for other users by limiting the number of concurrent run so that other people can also work. 

    **With great power, comes great responsibility.**

### More complex array jobs
In our previous example, we are only iterating one variable, which makes it rather easy to use the array job feature. 
However, what if we want to iterate more than one variable? 

!!! Warning
    I don't really recommend this. Once you need to iterate more than one parameter, you should really start using proper pipeline management.
    
    Though I guess when you don't have time commit to learning proper pipeline languages (which has a deep learning curve), this is the "poor people's" version of a "pipeline"

We can first write all combination of parameters into a single "parameter file".
Let's assume we have two phenotype files, `Eur.height` and `Eur.ldl`, and we want test a range of MAF: `0.001, 0.005, 0.01, 0.05, 0.1`. 
We can store the parameter combination in a `run.parameter` file as:
```
Eur.height  0.001
Eur.height  0.005
Eur.height  0.01
Eur.height  0.05
Eur.height  0.1
Eur.ldl  0.001
Eur.ldl  0.005
Eur.ldl  0.01
Eur.ldl  0.05
Eur.ldl  0.1
```
with 10 lines of information.

Our submission scripts will then be:
```bash
#BSUB -L /bin/sh      
#BSUB -n 1            
#BSUB -J PRS[1-10]     # 1 to 10 because our file has 10 line of parameter
#BSUB -q express      
#BSUB -W 12:00        
#BSUB -P acc_psychgen 
#BSUB -o prs.o%J.%I   
#BSUB -eo prs.e%J.%I  
#BSUB -M 20G          

# This will extrac the LSB_JOBINDEX th line from run.parameter and 
# store it as an array (note the outer () and the spaces between the () )
param=( $(sed -n ${LSB_JOBINDEX}p run.parameter) )
pheno=${param[0]}
maf=${param[1]}
project=/Users/sam/polygenic_score
# Run each analysis in their own unique folder
mkdir -p ${project}/analysis/prs/${pheno}-${maf}
# Run analysis in the specific file
cd ${project}/analysis/prs/${pheno}-${maf}
# Run the actual script
sh  ${project}/scripts/qc_and_run_prsice.sh \
    -p ${project}/software/plink \
    -b ${project}/data/EUR  \
    -c ${project}/data/EUR.cov \
    -r ${project}/software/PRSice \
    -s ${project}/data/Height.QC.gz \
    -f ${project}/data/${pheno} \
    -m ${maf} \
    -o ${pheno}-${maf}
```

The main "trick" here is to use `sed` to extract specific line from the parameter file, then tokenize the parameters into their own variable.

### Limitations
There are many limitations of this approach:

1. A lot of the input are hard coded. You need to make sure you know how many parameters you are running. Mispecify the job number and you will likely miss some of the jobs. 
2. It is really easy to accidentally run multiple instance in the same folder and lead to some kind of overwrite. Which might be difficult to track.
3. While it is managable when only a small amount of parameters are tested, it become increasingly difficult to check if all jobs have run to completion. 
4. If a job fails, there's no way for you to run from the failed step. You need to run the script from start to end. 


