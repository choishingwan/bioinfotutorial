# Background

Throughout my years as a bioinformatician, I have learn a lot of useful skills, which I hope will also be useful for others. 

!!! Note
    English is my second language. If you see any typo, please feel free to commit a correction to my repository.

!!! Warning
    I predominantly work on linux and mac. 
    As a result, this tutorial is not designed to be run on any window machine, unless you are using the linux subsystem.

## Objective
In this tutorial, we will try to introduce the followings

1. [Useful softwares](useful_software.md)
2. [Scripting 101](getting_started.md)



## Other resources

We have also written other tutorials 

1. [Polygenic risk score tutorial](https://choishingwan.github.io/PRS-Tutorial/)
2. [UK Biobank Management tutorial](https://choishingwan.gitlab.io/ukb-administration/)