# Introduction

Here are a list of software that I found useful, in no particular order

- [Visual Studio Code](https://code.visualstudio.com/)
    - A text editor 
    - In build [git](https://git-scm.com/) support
    - Have many useful extensions
        - [remote-ssh](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.remote-ssh): Can directly edit documents on server
- [R](https://www.r-project.org/)
    - Useful for plotting and carry out statistical analysis
    - [Rstudio](https://www.rstudio.com/) is a great IDE choice
    - My must have libraries are
        - `data.table`
            - Ultra powerful package that are efficient and allow many useful feature. 
            - Prefer this over tiddyverse due to smaller dependency requirement
            - Have a very steep learning curve
        - `magrittr`
            - Provide the piping ability
            - Allow more readable codes
        - `ggplot2`
            - Plotting, nothing more to say, this is just awesome
        - `forcats`
            - Have you got into troubles with `factor`? This makes `factor` "fun"
- [git](https://git-scm.com/)
    - For version control
    - Can store scripts on either [gitlab](gitlab.com/) or [github](github.com/)
        - I prefer [gitlab](gitlab.com/)
    - [gitkraken](https://www.gitkraken.com/) is a good software to help organizing your git
- [zotero](https://www.zotero.org/)
    - For citation management
- [iterm2](https://iterm2.com/)
    - Terminal on mac
    - Especially love its integration with `tmux`
- [MobaXterm](https://mobaxterm.mobatek.net/)
    - Terminal on windows
    - Didn't use it much, but heard good things about it
- [Nextflow](https://nextflow.io/)
    - Pipeline language
