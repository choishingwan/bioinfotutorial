# Introduction
In the previous sections, we have learn some basic scripting methods and exploiting the job submission system to achieve multi-parameter testing. However, we do notices a number of shortcomings, namely:

1. Can sometimes be difficult to organize our analysis
2. Can be difficult to track run status when large amount of parameters were tested
3. Cannot resume jobs if script fail at the very last step. Will need to re-run the whole analyses after debugging.
4. The more parameters involved, the more complicated our script becomes, with significant increase risk of bug. 

## Workflow languages
There are many different pipeline languages. In the field of bioinformatics, I think the most popular ones are Nextflow and snakemake:

<div style="text-align:center"><a href="https://nextflow.io/"><img src="https://www.nextflow.io/img/nextflow2014_no-bg.png" alt="nextflow" style="width:319px;"/></a></div>
<p></p>
<div style="text-align:center" background: #000><a href="https://snakemake.github.io/"><img src="https://github.com/snakemake/snakemake/blob/main/images/biglogo.png?raw=true" alt="snakemake" style="width:319px;"/></a></div>

<p></P>
There really isn't any right or wrong as to which language is better. 
The only thing matter is which language do **you** feel more comfortable with and can get the job done with less problem. For me, I only use Nextflow, for a number of reasons:

1. It is java. I don't need to worry about python version nor do I need to install `conda` on server. 
2. Very easy to install. Type `curl -s https://get.nextflow.io | bash` and you are done.
3. It uses a bottom up approach, rather than a top down approach, which is more intuitive for my style of coding
4. Most importantly, I tried `Nextflow` first, and got it running. As it serves most of what I did, I never really dive into `snakemake`

## Nextflow
I have been exclusively using the `DSL2` feature of `Nextflow`, which I think is a huge step up from previous version of `Nextflow` and have make things a lot easier for me. 
As a result of that, from now on, all of my comments on `Nextflow` will be based on my experience on using the `DSL2` feature, not the base `Nextflow` features. 

Based on my personal experience, here are the pros and cons of using `Nextflow`

### Pros
1. Once you understand how to use `Nextflow`, you can do very complex analyses in a very organized manner extremely efficiently.
2. Job submission is decoupled from the script and is automated by `Nextflow`. Essentially, your script will work on many different submission systems just by changing a single parameter.
3. I like having all my scripts organized in one place. The module system introduced by `DSL2` makes it really easy and I can reuse my scripts across different pipelines just by including the appropriate modules.  

### Cons

1. Not gonna lie, the biggest problem with `Nextflow` is its deep learning curve. It took me more than a week to learn how to use `Nextflow` and it took me even longer to really understand some of the concepts. I am still struggling with some functionality of `Nextflow` and I am sure there are still a lot of spaces where I can improve. 
2. While the document is comprehensive, you really need to know what you are looking for in order to find the require information. Which makes it a rather frustrating experience. Also, some of the document has not been updated for the `DSL2` feature, which I use exclusively. 
3. Some of the intermediates cannot be deleted (we will come to this later). This means that sometimes, we might sacrifice modularity to avoid generation of large intermediates. 
4. Error messages regarding the workflow is not informative. Most of the time, when you read the error message, you only know something is wrong, but you won't be able to figure out where or how just by reading the error message. 
5. I don't think I will use `Nextflow` if it was not because of the `DSL2` feature. Without the `DSL2` feature, you need to manually link different processes (will touch on this later), which make it both tedious and annoying.