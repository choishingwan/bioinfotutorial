# Introduction

In this section, we will try to perform a standard polygenic risk score analysis using `PRSice`.

You will need the followings:

!!! Note
    For now these data are stored on a dropbox account. We will need to move them to somewhere more permenant in the near future. 

## Learning Objective
1. Organize multiple script into a single file
2. Understand problem of relative path
3. Implement bash scripts that take user parameters


## Required data
- [GWAS summary statistics of height](https://www.dropbox.com/s/gciasd0b2pnbr83/Height.QC.gz?dl=0)
- [Genotype file](https://www.dropbox.com/s/d5jg1nvjsw2q181/EUR.bed?dl=0)
- [Sample file](https://www.dropbox.com/s/5odbg958hai833f/EUR.fam?dl=0)
- [Variant file](https://www.dropbox.com/s/3uad5m0n2om8h81/EUR.bim?dl=0)
- [Phenotype file](https://www.dropbox.com/s/bx0m7znvq36s4j6/EUR.height?dl=0)
- [Covariate file](https://www.dropbox.com/s/2akiofpjyx2fjjd/EUR.cov?dl=0)

## Required software
- [plink v1.90](https://www.cog-genomics.org/plink/)
- [PRSice v2.3.5](http://www.prsice.info/)

## Prepare your workspace

Before you start, we should first organize our workspace.
Having an organized workspace helps us to recover scripts and data whenever we need.

My current favourite organization are as follow 

```
- projects
  |
  | -- polygenic_score
       |
       | - scripts   # where we store all our scripts
       | - data      # where we store all our data
       | - analyses  # where we run our analyses
       |             # each analyses should have 
       |             # their own folder
       | - software  # where we store all software
                     # we can also generate a docker
                     # to help reproducibility
```

!!! Note
    
    You don't need to follow my structure. This is just how I like things to be and you can just use whatever that is sensible

So let's get started. Let's generate the required structure and put the data into the `data` folder and the software into the `software` folder

```bash
mkdir polygenic_score
cd polygenic_score
mkdir scripts
mkdir data
mkdir analyses
mkdir software
```

!!! tips
    You can either move the files into the folder using drag and drop (on local machine), or use the `mv` command. e.g. `mv ~/Download/EUR.bed polygenic_score/data/EUR.bed`

After moving all the files into the appropriate folders, let's also generate a folder call `standard_prs` under `analyses` and move to that folder

```bash
mkdir analyses/standard_prs
cd analyses/standard_prs
```

## Perform basic Quanlity control
Before we run PRSice, we will need to do some basic quality control on the data. We can run the following 
```bash
../../software/plink \
    --bfile ../../data/EUR \
    --maf 0.05 \
    --hwe 1e-6 \
    --geno 0.01 \
    --make-bed \
    --out EUR.QC
```

!!! tip
    Using `\` to separate each individual commands to a new line makes scripts more readable. 
    Unforunately, it also tends to introduce a special kind of bug: space behind `\`.
    
    Generally, if you have a space behind `\`, you have "escaped" the space instead of the newline, causing the script to assume there are no more commands to process.
    For example, if you have 
    ```
    ../../software/plink \ 
        --bfile ../../data/EUR
    ```
    this will be interpreted as running two separate commands: `../../software/plink` and `--bfile ../../data/EUR`. Therefore, the easiest way to debug this kind of error is to look for `-bash: XXX: command not found` where `XXX` is likely the command following the space after `\` bug.

This will generate `EUR.QC.bed`, `EUR.QC.bim` and `EUR.QC.fam` file. 

## Running PRSice
With the QCed data, we can now run PRSice with the following 
!!! note
    Replace `PRSice` with `PRSice_mac` or `PRSice_linux` w.r.t your system / how you name the PRSice executable

```bash
../../software/PRSice \
    --base ../../data/Height.QC.gz \
    --target EUR.QC \
    --binary-target F \
    --pheno ../../data/Eur.height \
    --cov ../../data/EUR.cov \
    --base-maf MAF:0.01 \
    --stat OR \
    --or \
    --out EUR
```

This will generate the `EUR.summary` file, which shows the PRS performance. 

## Combining the whole process
A common practice for script organization is to arrange all relevant scripts into a single file. 
This allow us to go back and check what analyses were performed to generate our reuslts, thus improve reproducibility. 
In this example, we would combine the QC and PRS calculation script into a single file:

```bash
#!/usr/bin/env bash
../../software/plink \
    --bfile ../../data/EUR \
    --maf 0.05 \
    --hwe 1e-6 \
    --geno 0.01 \
    --make-bed \
    --out EUR.QC

../../software/PRSice \
    --base ../../data/Height.QC.gz \
    --target EUR.QC \
    --binary-target F \
    --pheno ../../data/Eur.height \
    --cov ../../data/EUR.cov \
    --base-maf MAF:0.01 \
    --stat OR \
    --or \
    --out EUR
```

We can put this file in the `script` folder of our project directory, and maybe give it a name: `qc_and_run_prsice.sh`

!!! Note
    We put `#!/usr/bin/env bash` as the first line of the file. 
    This tells our operation system that this file is a bash script, and will allow us to run this script by typing `sh qc_and_run_prsice.sh` to run the script. 

## Problem of relative path
Now that we have a running script, we can start slowly improving the content of this script. 

First off, all of our path are currently hard-coded using relative paths. That is, this script will only work in folders that are two folder deep relative to the `software` and `data` folder. 

We can try and make it more robust by first defining a project path on the top of the script, and then use the **absolute** path instead of the **relative** path:


```bash
#!/usr/bin/env bash

project=/Users/sam/polygenic_score

${project}/software/plink \
    --bfile ${project}/data/EUR \
    --maf 0.05 \
    --hwe 1e-6 \
    --geno 0.01 \
    --make-bed \
    --out EUR.QC

${project}/software/PRSice \
    --base ${project}/data/Height.QC.gz \
    --target EUR.QC \
    --binary-target F \
    --pheno ${project}/data/Eur.height \
    --cov ${project}/data/EUR.cov \
    --base-maf MAF:0.01 \
    --stat OR \
    --or \
    --out EUR
```

Now, as long as your project folder is put under `/Users/sam/`, this script will work anywhere on the system. 

!!! tip
    In bash, both `$project` and `${project}` are valid syntax for a variable. 
    The main difference is that in scenario where you want to append something starts with `_` to the end of the variable:
    ```bash
    name="hi"
    echo $name_how_are_you
    echo ${name}_how_are_you
    ```
    bash will assume `$name_how_are_you` is a variable, whereas `${name}_how_are_you` is the variable `${name}` with `_how_are_you` appended to its output.

    It is also a good idea to clearly show reader what your variables are. As such, I almost always use `${variable}` instead of `$variable`

## Using positional arguments

However, this script still assume a folder structure, which means if someone want to replicate your analysis, but their folder structure is different, this script will break. 
In addition, if they store the data or software with a different name, this script will also fail. 
We can make this script more robust by accepting user parameters instead


```bash
#!/usr/bin/env bash
plink=${1}
bfile=${2}
prsice=${3}
sumstat=${4}
pheno=${5}
cov=${6}

${plink} \
    --bfile ${bfile} \
    --maf 0.05 \
    --hwe 1e-6 \
    --geno 0.01 \
    --make-bed \
    --out EUR.QC

${prsice} \
    --base ${sumstat} \
    --target EUR.QC \
    --binary-target F \
    --pheno ${pheno} \
    --cov ${cov} \
    --base-maf MAF:0.01 \
    --stat OR \
    --or \
    --out EUR
```

Now, we would be able to run the `qc_and_run_prsice.sh` script with the following syntax
```bash
project=/Users/sam/polygenic_score
sh qc_and_run_prsice.sh \
    ${project}/software/plink \
    ${project}/data/EUR \
    ${project}/software/PRSice \
    ${project}/data/Height.QC.gz \
    ${project}/data/Eur.height \
    ${project}/data/EUR.cov
```

## Using OPTARG flags
The positional argument allow users to provide the require software and data great flexibility, yet unless users read your documentation, or read your script, it will be difficult for them to know the order of the input. 
This might also easily lead to silent bugs, where you provided the wrong file for the process, for example, in our mock test, the script should still run if someone accidentally swarp the ordering of the phenotype and covariate files. 

To make the script more readable, we can instead use `getopts` from bash

```bash
#!/usr/bin/env bash
while getopts p:b:r:s:f:c: flag
do
    case "${flag}" in
        p) plink=${OPTARG};;
        b) bfile=${OPTARG};;
        r) prsice=${OPTARG};;
        s) sumstat=${OPTARG};;
        f) pheno=${OPTARG};;
        c) cov=${OPTARG};;
    esac
done

${plink} \
    --bfile ${bfile} \
    --maf 0.05 \
    --hwe 1e-6 \
    --geno 0.01 \
    --make-bed \
    --out EUR.QC

${prsice} \
    --base ${sumstat} \
    --target EUR.QC \
    --binary-target F \
    --pheno ${pheno} \
    --cov ${cov} \
    --base-maf MAF:0.01 \
    --stat OR \
    --or \
    --out EUR
```

We can now use  `qc_and_run_prsice.sh` as follow:
```bash
project=/Users/sam/polygenic_score
sh qc_and_run_prsice.sh \
    -p ${project}/software/plink \
    -b ${project}/data/EUR  \
    -c ${project}/data/EUR.cov \
    -r ${project}/software/PRSice \
    -s ${project}/data/Height.QC.gz \
    -f ${project}/data/Eur.height
```

and we can freely shift the input ordering and still get a valid script.

## Adding usage message
It is always a good idea to provide documentation to your script. So lets put in a usage message to your script so users can understand what `-p` and the other flags are expecting. 

```bash
#!/usr/bin/env bash
usage(){
    cat << EOF
Usage: ${0}
Run a simple polygenic risk score pipeline.
It will first filter out SNPs with MAF < 0.05, hwe < 1e-6 and genotype missingness > 0.01, then run PRSice using the provided summary statistic, phenotype and all covariates in the covariate file

    -p    PLINK executable
    -b    Genotype file prefix
    -r    PRSice executable
    -s    Summary statistic file with the following columns: SNP, P, OR, A1 and A2
    -f    Phenotype file. Assume third column is the phenotype
    -c    Covariate file. All covariates in this file will be used
    -o    Output prefix
    
EOF
}
while getopts p:b:r:s:f:o:c:h flag
do
    case "${flag}" in
        p) plink=${OPTARG};;
        b) bfile=${OPTARG};;
        r) prsice=${OPTARG};;
        s) sumstat=${OPTARG};;
        f) pheno=${OPTARG};;
        c) cov=${OPTARG};;
        o) prefix=${OPTARG};;
        h) usage
        exit 0
        ;;
    esac
done

${plink} \
    --bfile ${bfile} \
    --maf 0.05 \
    --hwe 1e-6 \
    --geno 0.01 \
    --make-bed \
    --out ${prefix}.QC

${prsice} \
    --base ${sumstat} \
    --target ${prefix}.QC \
    --binary-target F \
    --pheno ${pheno} \
    --cov ${cov} \
    --base-maf MAF:0.01 \
    --stat OR \
    --or \
    --out ${prefix}
```
!!! Note
    I have added a prefix parameter (`-o`) here, which allow user to specify what output prefix they'd like. 
    This should prevent accidentally overwriting results from previous run 

With this new change, users can see what are the expected input by typing `sh qc_and_run_prsice.sh -h`. 

!!! Note
    In the `getopts` string, variable with follow by a `:` indicate that they require an argument (e.g. `p:` means for `-p`, we expect it is followed by a user input, which is captured in `${OPTARG}`). Variable without `:` means they don't expect an input e.g. `-h`


## Optimization
We now have a functional and well documented script, and in theory, this script should work as long as the input file satisfy the requirement. 
Yet, if we have a large dataset (e.g. UK Biobank), this script will use up a lot of disk spaces because we are writing a new genotype file after the QC step.

We can significantly improve the storage requirement of this script using `--write-snplist` and `--make-just-fam` parameter from `plink`, and use `--keep`, `--remove`, `--extract` and `--exclude` parameter in `PRSice` to avoid duplicating the large genotype bed file. 

We can also compress the *best score* output, which can be big for large sample size dataset. 

The updated script is now


```bash
#!/usr/bin/env bash
usage(){
    cat << EOF
Usage: ${0}
Run a simple polygenic risk score pipeline.
It will first filter out SNPs with MAF < 0.05, hwe < 1e-6 and genotype missingness > 0.01, then run PRSice using the provided summary statistic, phenotype and all covariates in the covariate file

    -p    PLINK executable
    -b    Genotype file prefix
    -r    PRSice executable
    -s    Summary statistic file with the following columns: SNP, P, OR, A1 and A2
    -f    Phenotype file. Assume third column is the phenotype
    -c    Covariate file. All covariates in this file will be used
    -o    Output prefix
    
EOF
}
while getopts p:b:r:s:f:o:c:h flag
do
    case "${flag}" in
        p) plink=${OPTARG};;
        b) bfile=${OPTARG};;
        r) prsice=${OPTARG};;
        s) sumstat=${OPTARG};;
        f) pheno=${OPTARG};;
        c) cov=${OPTARG};;
        o) prefix=${OPTARG};;
        h) usage
        exit 0
        ;;
    esac
done

${plink} \
    --bfile ${bfile} \
    --maf 0.05 \
    --hwe 1e-6 \
    --geno 0.01 \
    --make-just-fam \
    --make-snplist \
    --out ${prefix}.QC

${prsice} \
    --base ${sumstat} \
    --target ${bfile} \
    --binary-target F \
    --pheno ${pheno} \
    --cov ${cov} \
    --base-maf MAF:0.01 \
    --stat OR \
    --or \
    --out ${prefix} \
    --keep ${prefix}.QC.fam \
    --extract ${prefix}.QC.snplist

gzip  ${prefix}.best
```

Always optimize your script **after** you have tested that the script works, unless it is very simple e.g. compressing output file.

It is my personal opinion that one should always be mindful of your resource usage and optimize accordingly.
Think carefully what files are required / can be compressed, and delete anything you don't need as soon as possible.
That can avoid clutering the server and can sometimes even improve the performance of your scripts as repeatedly reading / writing large files can be timme consuming. 